#pragma once
#include <iostream>
#include <cstddef>
#include <algorithm>

template<typename T>
class vector
{
    T *ptr;
    std::size_t s, cap;

    void reallocate(std::size_t n);

public:
    ~vector()
    {
        for(std::size_t i = 0; i < this->s; ++i)
            (ptr + i)->~T();
        std::free(ptr);
    }
    vector()
        : ptr(nullptr)
        , s(0)
        , cap(0)
    {}
    vector(std::size_t n)
        : vector<T>()
    {
        reallocate(n);
        for(std::size_t i = 0; i < n; i++){
            new (ptr + i) T();
        }
        this->s = n;
    }
    vector(vector<T> const &x)
        : vector<T>()
    {
        reallocate(x.s);
        this->s= x.s;
        for(std::size_t i = 0; i < x.s; i++){
            new (ptr + i) T(x[i]);
        }
    }
    vector(vector<T>      &&x) 
        : ptr(x.ptr)
        , s(x.s)    
        , cap(x.cap)
    {
        new (&x) vector<T>();
    }

    vector(std::initializer_list<T> list)//{}
        : vector<T>()
    {
        std::size_t n = list.size();
        reallocate(n);
        for (std::size_t i = 0; i < n; i++){
            new (ptr + i) T(static_cast<T>(*(list.begin() + i)));
        }
        this->s = n;
    }

    vector<T> &operator=(vector<T> const &other)//copy
    {
        if( this != &other){
            this->~vector<T>();
            new (this) vector<T>(other);
        }
        return *this;
    }
    vector<T> &operator=(vector<T>      &&other)//move
    {
        if( this != &other){
            this->~vector<T>();
            new (this) vector<T>(static_cast<vector<T> &&>(other));
        }
        return *this;
    }

    bool operator==(const vector<T> &other) const {
        if(this->s== other.s){
            for(std::size_t i = 0; i < this->s; i++){
                if( ptr[i] != other.ptr[i] ){
                    return 0;
                }
            }
            return 1;
        }
        else{
            return 0;
        }
    }

    bool operator!=(const vector<T> &other) const {
        if(this->s== other.s){
            for(std::size_t i = 0; i < this->s; i++){
                if( ptr[i] != other.ptr[i] ){
                    return 1;
                }
            }
        }
        else{
            return 1;
        }
        return 0;
    }

    bool operator<(const vector<T> &other) const {
        std::size_t n = other.s;
        if(*this != other){
            if(n < this->s){
                for(std::size_t i = 0; i < n; i++){
                    if(other.ptr[i] > ptr[i]){
                        return 1;
                    }
                    else{
                        if(ptr[i] > other.ptr[i]){
                            return 0;
                        }
                    }
                }
                return 0;
            }
            else{
                for(std::size_t i = 0; i < this->s; i++){
                    if(other.ptr[i] > ptr[i]){
                        return 1;
                    }
                    else{
                        if(ptr[i] > other.ptr[i]){
                            return 0;
                        }
                    }
                }
                return 0;
            }
        }
        else{
            return 0;
        }
    }

    bool operator>(const vector<T> &other) const {
        std::size_t n = other.s;
        if(*this != other){
            if(n < this->s){
                for(std::size_t i = 0; i < n; i++){
                    if(other.ptr[i] > ptr[i]){
                        return 0;
                    }
                    else{
                        if(ptr[i] > other.ptr[i]){
                            return 1;
                        }
                    }
                }
                return 1;
            }
            else{
                for(std::size_t i = 0; i < this->s; i++){
                    if(other.ptr[i] > ptr[i]){
                        return 0;
                    }
                    else{
                        if(ptr[i] > other.ptr[i]){
                            return 1;
                        }
                    }
                }
                return 1;
            }
        }
        else{
            return 0;
        }
    }

    bool operator<=(const vector<T> &other) const {
       if(*this < other) {return 1;}
       if(*this == other){return 1;}
       return 0;
    }

    bool operator>=(const vector<T> &other) const {
        if(*this > other) {return 1;}
        if(*this == other){return 1;}
        return 0;
    }

    T       *begin()       {return ptr;}
    T       *  end()       {return ptr + this->s;}
    T const *begin() const {return ptr;}
    T const *  end() const {return ptr + this->s;}

    void push_back(T const &da)
    {
        if(this->s== cap)
            reallocate(cap == 0 ? 1 : 2 * cap);
        new (ptr + this->s++) T(da);
    }

    T       &front()       {return *ptr;}
    T const &front() const {return *ptr;}

    T       &back()       {return ptr[this->s- 1];}
    T const &back() const {return ptr[this->s- 1];}

    T       *data()       {return ptr;}
    T const *data() const {return ptr;}
    
    T       &operator[](std::size_t n)       {return ptr[n];}
    T const &operator[](std::size_t n) const {return ptr[n];}
    T       &operator[](int n)       {return ptr[n];}
    T const &operator[](int n) const {return ptr[n];}

    T       &at(std::size_t n)       {return ptr[n];}
    T const &at(std::size_t n) const {return ptr[n];}
    
    T       *rbegin()       {return ptr + this->s;}
    T       *  rend()       {return ptr;}
    T const *rbegin() const {return ptr + this->s;}
    T const *  rend() const {return ptr;}

    bool empty() const {return this->s== 0;}

    std::size_t size() const {return this->s;}

    std::size_t max_size() const {return cap;}

    void reserve(std::size_t new_cap){
        if(new_cap > cap)
            reallocate(new_cap);
    }

    std::size_t capacity() const {return cap;}

    void shrink_to_fit(){
        reallocate(s);
    }

    void erase(T *pos){
        std::size_t n = std::size_t(this->end() - pos);
        for(std::size_t i = 0; i < n - 1; i++){
            pos[i] = static_cast<T &&>(pos[i + 1]);
        }
        (this->end() - 1)->~T();
        this->s--;
    }

    void erase(T *a, T *b){
        std::size_t n = std::size_t(this->end() - b);
        for(std::size_t i = 0; i < n; i++){
            a[i] = static_cast<T &&>(b[i]);
        }
        for(std::size_t i = 0; i < std::size_t(b - a); i++){
            (this->end() - i - 1)->~T();
        }
        this->s-= std::size_t(b - a);
    }

    void clear(){
        erase(begin(), end());
    }

    void pop_back(){
        (ptr + this->s- 1)->~T();
        this->s--;
    }

    void insert(const T *pos, T &&value){
        std::size_t n = std::size_t(pos - ptr);
        if(this->s== cap){
            reallocate(cap == 0 ? 1 : 2 * cap);
        }
        new (ptr + this->s) T();

        T *newpos = ptr + n;

        for(std::size_t i = 0; i < std::size_t(this->end() - newpos); i++){
            ptr[this->s- i] = static_cast<T &&>(ptr[this->s- i - 1]);
        }
        ptr[n] = value;
        // ptr[s] = value;
        this->s++;
    }

    void insert(const T *pos, const T &value){
        std::size_t n = std::size_t(pos - ptr);
        if(this->s== cap){
            reallocate(cap == 0 ? 1 : 2 * cap);
        }

        new (ptr + this->s) T();

        T *newpos = ptr + n;

        for(std::size_t i = 0; i < std::size_t(this->end() - newpos); i++){
            ptr[this->s- i] = static_cast<T &&>(ptr[this->s- i - 1]);
        }

        ptr[n] = value;

        // ptr[s] = value;

        this->s++;

    }

    void insert(const T *pos, const T *first, const T *last){
        std::size_t n = std::size_t(pos - ptr);
        std::size_t m = std::size_t(last - first);
        if((this->s+ m) > cap){
            reallocate(cap + m);
        }
        for (std::size_t i = 0; i < m; i++){
            new (ptr + this->s+ i) T();
        }

        T *newpos = ptr + n;

        for(std::size_t i = 0; i < std::size_t(this->end() - newpos); i++){
            ptr[this->s- i + m - 1] = static_cast<T &&>(ptr[this->s- i - 1]);
        }

        for(std::size_t i = 0; i < m; i++){
            newpos[i] = first[i];
        }
        // ptr[s] = value;

        this->s+= m;

    }

    void insert(const T *pos, const std::size_t &count, const T &value){
        std::size_t n = std::size_t(pos - ptr);
        if((this->s+ count) > cap){
            reallocate(cap + count);
        }
        for (std::size_t i = 0; i < count; i++){
            new (ptr + this->s+ i) T();
        }

        T *newpos = ptr + n;

        for(std::size_t i = 0; i < std::size_t(this->end() - newpos); i++){
            ptr[this->s- i + count - 1] = static_cast<T &&>(ptr[this->s- i - 1]);
        }

        for(std::size_t i = 0; i < count; i++){
            newpos[i] = value;
        }
        // ptr[s] = value;

        this->s+= count;

    }

    void insert(const T *pos, const std::initializer_list<T> list){
        std::size_t n = std::size_t(pos - ptr);
        std::size_t m = std::size_t(list.size());
        if((this->s+ m) > cap){
            reallocate(cap + m);
        }
        for (std::size_t i = 0; i < m; i++){
            new (ptr + this->s+ i) T();
        }

        T *newpos = ptr + n;

        for(std::size_t i = 0; i < std::size_t(this->end() - newpos); i++){
            ptr[this->s- i + m - 1] = static_cast<T &&>(ptr[this->s- i - 1]);
        }

        for(std::size_t i = 0; i < m; i++){
            newpos[i] = *(list.begin() + i);
        }
        // ptr[s] = value;

        this->s+= m;

    }

    void swap(vector<T> &other){
        std::swap(ptr, other.ptr);
        std::swap(s, other.s);
        std::swap(cap, other.cap);
    }

    template<typename... Args>
    T *emplace(const T *pos, Args &&... args){
        std::size_t n = std::size_t(pos - ptr);


        if(this->s== cap){
            reallocate(cap == 0 ? 1 : 2 * cap);
        }

        new (ptr + this->s) T();

        T *newpos = ptr + n;
        if(s != 0){
            for(std::size_t i = 0; i < std::size_t(this->end() - newpos); i++){
                ptr[this->s- i] = static_cast<T &&>(ptr[this->s- i - 1]);
            }
        }
        (ptr + n)->~T();
        new (ptr + n) T(std::forward<Args &&>(args)...);
        // ptr[n] = T(static_cast<Args &&...>(args...));

        // ptr[s] = value;

        this->s++;
        return (ptr + n);
    }

    template<typename... Args>
    T *emplace_back(Args &&... args){
        return emplace(end(), std::forward<Args &&>(args)...);
    }

    void assign(std::size_t count, const T &value){//works
        clear();
        if(count > cap){
            reallocate(count);
        }
        insert(ptr, count, value);
    }

    void assign(const T *first, const T *last){
        clear();
        std::size_t n = std::size_t(last - first);
        if(n > cap){
            reallocate(n);
        }
        insert(ptr, first, last);
    }

    void assign(std::initializer_list<T> list){//works
        clear();
        std::size_t n = std::size_t(list.size());
        if(n > cap){
            reallocate(n);
        }
        insert(ptr, list);
    }
};

template<typename T>
void vector<T>::reallocate(std::size_t n){
    T *newptr = static_cast<T *>(std::malloc(sizeof(T)*n));
    for(std::size_t i=0; i<s; i++){
        new (newptr + i) T(static_cast<T &&>(ptr[i]));
        (ptr + i)->~T();
    }   
    std::free(ptr);
    ptr = newptr;
    cap = n;
}
